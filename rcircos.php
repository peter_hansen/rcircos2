<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"> </script>
<script src="http://malsup.github.com/jquery.form.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Circos Generator</title>
<link href="rcircos.css" rel="stylesheet" type="text/css" />
<script type="text/css">
td {
	border:1px solid #ddd;
	white-space: nowrap;
}

</script>
<script type="text/javascript">
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function cookieAjax() {
	return 		$.ajax({
		type: "POST",
		url: "userSearch.php",
		data: { key: get_cookie('rcircostoken')},
	});
}
$(document).ready(function() {
	// check if the user has a cookie and if they do use it as the ID
	if(get_cookie('rcircostoken') != '') {
		var promise = cookieAjax();
		promise.success(function (data) {
			$('#seed_div').html(data);
		});
//		var dis = document.getElementById('userID');
//		var token = get_cookie('rcircostoken');
//		dis.type = "hidden";
//		dis.value = token;
//		dis = document.getElementById('username');
//		dis.value = token;
//		$("#ID").html(token);
//		$("#cookie").html("<button type='button' onclick='Javascript: forgetCookie();'>Forget Cookie</button>");
	}
	var deloptions = {
			target:			'#delete_div',
			url: 'delFiles.php',
	};
	/* when the user clicks the delete files button the form is submited to
		a file which handles the server side deletions and hiding the
		row elements that were deleted*/
	$('#deleteFiles').click(function() {
		$('#idForm').ajaxSubmit(deloptions);
	});
	/* when the user clicks the start button it hides the data upload form 
		and displays the plot making form */
	$('#start').click(function() {
		start();
		$('#idForm').css('display','none');
		$('#circosForm').css('display', 'block');
	});
	var optionsinit = { 
	        target:        '#seed_div',   // target element to be updated with server response 
	        url: 'fileupload.php',
	    }; 
    /* when the user clicks the data upload button the form is submitted to
    	a file which creates the appropriate files in the directory
    	and creates the table showing the uploaded files */
	$('#idForm').submit(function() { 
		$('#start').css('display', 'inline');
	    $(this).ajaxSubmit(optionsinit); 
	    return false; 
	});
    var options = { 
        target:        '#circos_div',   // target element to be updated with server response 
    }; 
    /* when the user clicks the start button on the second form the data is 
    	submitted to a file which creates the plot and displays it underneath
    	the form. While it is processing a spinner appears */
    $('#circosForm').submit(function() {
    	document.getElementById('circos_div').innerHTML = '<img src=\'images/spinner2-greenie.gif\'   style=\'position:relative; left:0px;\' height=\'30\' width=\'30\' />';
        $(this).ajaxSubmit(options); 
        return false; 
    });
}); 
</script>
<script type="text/javascript">
/* a function to make handling cookies easier. It takes cookie info
 * and then creates a cookie based on that if it is valid as well as
 * converting the usual seconds into days
 */
function set_cookie ( cookie_name, cookie_value, lifespan_in_days, valid_domain ) {
	    // http://www.thesitewizard.com/javascripts/cookies.shtml
	    var domain_string = valid_domain ?
	                       ("; domain=" + valid_domain) : '' ;
	    document.cookie = cookie_name +
	                       "=" + encodeURIComponent( cookie_value ) +
	                       "; max-age=" + 60 * 60 *
	                       24 * lifespan_in_days +
	                       "; path=/" + domain_string ;
	}
/* a function to make handling cookies easier. It takes cookie info 
 * and returns any information stored as a cookie that matches that
 * info. If the cookies does not exits it returns an empty string 
 */
function get_cookie ( cookie_name )
{
    // http://www.thesitewizard.com/javascripts/cookies.shtml
    var cookie_string = document.cookie ;
    if (cookie_string.length != 0) {
        var cookie_value = cookie_string.match (
                        '(^|;)[\s]*' +
                        cookie_name +
                        '=([^;]*)' );
        return decodeURIComponent ( cookie_value[2] ) ;
    }
    return '' ;
}
/* a function to make handling cookies easier. It takes cookie info
 * and if that cookie exits it deletes it
 */
function delete_cookie ( cookie_name, valid_domain )
{
    // http://www.thesitewizard.com/javascripts/cookies.shtml
    var domain_string = valid_domain ?
                       ("; domain=" + valid_domain) : '' ;
    document.cookie = cookie_name +
                       "=; max-age=0; path=/" + domain_string ;
}
/* a function that takes the number of the track that needs to be changed
 * and then changes the download sample data link to the file that matches
 * the selected type of track.
 */
function trackDownload(num) {
	"use strict";
	var trackType = 'trackType' + num;
	var trackDiv = "track" + num +"_div";
	var doc = document.getElementById(trackType);
	var type = doc.options[doc.selectedIndex].value;
	var newDownload = "<a href = \"rcircos_help.html\" onclick=\"Javascript: return popitup('rcircos_help.html#" + type + "')\">?&nbsp;</a> <a href=\"Sampledata/example" + type + "Data.txt\">Download Data</a>";
	document.getElementById(trackDiv).innerHTML = newDownload;
}
/* This function makes sure that the entered track height does not exceed 3
 * since the plot does not come out correctly if it does
 */
function fixHeight() {
	trackHeight = document.getElementById('trackHeight');
	if(trackHeight.value > 3) {
		trackHeight.value = 3;
		alert("Warning! Track Height cannot be greater than 3!");
	}
}
/* This function ensures that no illegal characters will be included in the
 * plot name to prevent errors
 */
function fixName() {
	var illegal = new Array('/', '\\', '(', ')', '.', '~', "'", '"', "?", "#", "$", "%", "&", ",", "!", "@", "^", "*", ";", "[", "]", "{", "}", "<",">","+" );
	var name = $('#plotName').val();
  	var error = false;
	for(var i = 0; i<26; i++) {
		var charExists = (name.indexOf(illegal[i]) >= 0);
		if(charExists) {
			error = true;
			var character = illegal[i];
			var rnum = name.split(character);
			var len = rnum.length;
			name = '';
			for(var c = 0; c<len; c++) {
				name += rnum[c];
			}
		}
	}
	if(error) {
		$('#plotName').val(name);
		window.alert('Illegal characters have been removed from the plot name');
	}
}
/* This function creates the pop up windows that the help information shows up in
 * and shifts the focus to the new window.
 */
function popitup(url) {
	newwindow=window.open(url, 'name', 'height=600, width=800, scrollbars=yes');
	if(window.focus) {newwindow.focus()}
	return false;
}
/* HTML's reset function does not work properly with some elements of the form
 * so this manually resets the visability of chromosomes and download data links
 */
function speciesReset() {
	document.getElementById('track3_div').innerHTML = "<a href='Sampledata/exampleLineData.txt'>Download Data</a>";
	document.getElementById('Chr20').style.display = "table-cell";
	document.getElementById('Chr21').style.display = "table-cell";
	document.getElementById('Chr22').style.display = "table-cell";
	document.getElementById('ChrY').style.display = "table-cell";
	document.getElementById('track1_div').innerHTML = "<a href='Sampledata/exampleHeatmapData.txt'>Download Data</a>";
	document.getElementById('track2_div').innerHTML = "<a href='Sampledata/exampleHistogramData.txt'>Download Data</a>";
	document.getElementById('track4_div').innerHTML = "<a href='Sampledata/exampleScatterData.txt'>Download Data</a>";
	document.getElementById('track5_div').innerHTML = "<a href='Sampledata/exampleTileData.txt'>Download Data</a>";
}
/* Since each species has different chromosomes the chromosome exclude table 
 * must be modified to acurately reflect the chromosomes that the species has.
 * This function simply hides or shows the ones that are supposed to be there. 
 */
function changeSpecies() {
	var speciesName = document.getElementById('speciesName');
	if(speciesName.value == "Human") {
		document.getElementById('Chr20').style.display = "table-cell";
		document.getElementById('Chr21').style.display = "table-cell";
		document.getElementById('Chr22').style.display = "table-cell";
		document.getElementById('ChrY').style.display = "table-cell";
	} else if(speciesName.value == "Mouse") {
		document.getElementById('Chr20').style.display = "none";
		document.getElementById('Chr21').style.display = "none";
		document.getElementById('Chr22').style.display = "none";
		document.getElementById('ChrY').style.display = "table-cell";
	} else {
		document.getElementById('Chr20').style.display = "table-cell";
		document.getElementById('Chr21').style.display = "none";
		document.getElementById('Chr22').style.display = "none";
		document.getElementById('ChrY').style.display = "none";
	}
}
/* Generates a random 30 character long string that serves as the identifier 
 * for each unique user. The server creates a directory for this id and then
 * displays the id in the textbox and gives the option to save the id as a cookie
 */
function startNewSession() { 
	delete_cookie('rcircostoken');
	$('#fileManager').css('display', 'none');
	var chars = "1234567890abcdefghijklmnopqrstuvwxyz";
	var token = "";
	var c = 0;
	while(c <= 30) {
		token = token.concat(chars.charAt(Math.random()*47));
		c++;
	}
	$.ajax({
		type: "POST",
		url: "makeDirectory.php",
		data: { username: token}
	}).done(function() {
		var dis = document.getElementById('userID');
		dis.value = token;
		$('#username').val(token);
		dis.type = "text";
		$("#ID").html('');
		$('#username').val(token);
		$("#cookie").html("<button type='button' onclick='Javascript: saveCookie();'>Save as Cookie</button>");
	});

}
function saveCookieAjax() {
	return 		$.ajax({
		type: "POST",
		url: "hash.php",
		data: { plaintext : document.getElementById('userID').value},
	});
}
/* saves the id in the textbox as a cookie that will by default apear whenever
 * the user returns to the site and takes away the textbox for id input.
 */
function saveCookie() {
	 var promise = saveCookieAjax();
	promise.success(function (data) {
		$('#circos_div').html(data);
	});
//	var dis = document.getElementById('userID');
//	dis.type = "hidden";
//	$("#ID").html(dis.value);
//	set_cookie('rcircostoken', dis.value, 14);
//	$("#cookie").html("<button type='button' onclick='Javascript: forgetCookie();'>Forget Cookie</button>");
}
/* 
 * deletes the user's cookie and returns the function to input a user provided id 
 */
function forgetCookie() {
	var dis = document.getElementById('userID');
	dis.type = "text";
	dis.value = $('#ID').html();
	$("#ID").html('');
	delete_cookie('rcircostoken');
	$("#cookie").html("<button type='button' onclick='Javascript: saveCookie();'>Save as Cookie</button>");
}
/* 
 * Switches the display to the currently selected plot from the hitory menu
 */
function histUpdate() {
	var username = $('#userID').val();
	var circosPlot = $('#historySelect').val() ;
	if(circosPlot.slice(-3) == 'pdf') {
		$('#circos_div').html("<a href='" + circosPlot + "'>Download Plot</a>");
	} else {
		$('#circos_div').html("<img align='center' src='" + circosPlot +"' height='800' width='800' >");
	}
}
/*
 * Draws a line given start and stop coordinates
 */
function DrawLine(x1, y1, x2, y2, color) {
	var canvas = document.getElementById('relationsCanvas');
	var context = canvas.getContext('2d');
	var posAdjust = $('#relationsCanvas').offset();
	x1 = x1-posAdjust.left;
	y1 = y1-posAdjust.top;
	x2 = x2-posAdjust.left;
	y2 = y2-posAdjust.top;
	context.beginPath();
	context.lineWidth = 5;
	context.moveTo(x1, y1);
	context.quadraticCurveTo(250 , y2 , x2, y2);
    context.strokeStyle = color;
	context.stroke(); 
	}
/*
 * Espaces characters that need to be escaped in a jquery ID selector
 */
function jq( myid ) {
	return myid.replace( /(:|\.|\[|\])/g, "\\$1" );
}
</script>
</head>
<body>
<!--[IF IE]>
    <script type="text/javascript">
           <script>window.alert('WARNING Internet explorer does not support many of the features on this website, please consider upgrading your browser');</script>
    </script>
<![endif]-->
<div id='delete_div'></div>
<div align="center">
<h1> Circos Plot Generator</h1> <a href="http://cran.r-project.org/web/packages/RCircos/index.html" target="_blank">A web interface for the RCircos r package</a><br>
Questions? Comments? Contact peter.hansen.nih@gmail.com
</div>
<div id="user_div" name="user_div" align=center></div>
<form id ="idForm" name="idForm" method="post" style="display: block">
<table width="600" align="center" cellpadding='3' cellspacing='2' style='border: 1px solid #CCCCCC;background-color:#DEEBDC; '>
  	<tr>
    	<td width="1">&nbsp;</td>
    	<td width="159">&nbsp;</td>
    	<td width="184"><button type=button id="session" onclick="Javascript: startNewSession(); var dis = document.getElementById('session'); dis.style.display = 'none';">Start New Session</button></td>
    	<td width="131">&nbsp;</td>
    	<td width="7">&nbsp;</td>
  	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><div align="right">
				<strong>User ID:</strong>
			</div>
		</td>
		<td><input name="userID" type="text" id="userID" onchange="Javascript: $('#username').val($('#userID').val());"><div id="ID"></div></td>
		<td id="cookie"><button type='button' onclick='Javascript: saveCookie();'>Save as Cookie</button></td>
	</tr>
 			<tr><td>&nbsp;</td> <td colspan=8><div id="seed_div"></div> </td> </tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file1" id="file1" onchange="Javascript: dis=document.getElementById('track2row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
	 		<tr id="track2row" style="display: none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file2" id="file2" onchange="Javascript: dis=document.getElementById('track3row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>	
	 	 	<tr id="track3row" style="display:none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file3" id="file3" onchange="Javascript: dis=document.getElementById('track4row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>	
			
			<!-- Track 4 data upload -->
			<tr id="track4row" style="display: none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file4" id="file4" onchange="Javascript: dis=document.getElementById('track5row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
			<!-- Track 5 data upload -->
			<tr id="track5row" style="display: none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file5" id="file5" onchange="Javascript: dis=document.getElementById('track6row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
			<tr id="track6row" style="display:none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file6" id="file6" onchange="Javascript: dis=document.getElementById('track7row'); dis.style.display='table-row';">
				<td>&nbsp;</td>
			</tr>
			<tr  id="track7row" style="display:none">
				<td>&nbsp;</td>
				<td colspan=6 align="center"><input type="file" name="file7" id="file7">
				<td>&nbsp;</td>
			</tr>
			<tr>
		<td colspan=6 align=center><INPUT type="Submit" Name=Submit VALUE="Upload data" id="mySubmit"></td>
		<tr> <td colspan=6 align=center>
		<button type='button' id='start' style="display: none">Start</button>
  </td>
	</tr>
</table>
</form>

<form id="circosForm" name="circosForm" method="post" action="rcircos_action.php" enctype="multipart/form-data" style="display: none">
<!-- 
Initialize table
 -->
<table width="800" align="center" cellpadding='3' cellspacing='2' style='border: 1px solid #CCCCCC;background-color:#DEEBDC; '>
  <tr>
    <td width="1"><input name="username" type="hidden" id="username"></td>
    <td width="159">&nbsp;</td>
    <td width="184">&nbsp;</td>
    <td width="80">&nbsp;</td>
    <td width="7">&nbsp;</td>
  </tr>
 <!-- 
 Species data input
  -->
			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Species:</strong>
					</div></td>
				<td><select name='speciesName' id='speciesName' onchange="Javascript: changeSpecies()">
						<option value="Human" selected='selected'>Human (hg19)</option>
						<option value="Mouse">Mouse (mm9)</option>
						<option value="Rat">Rat (rn4)</option>
				</select></td>
				<td><a href="rcircos_help.html" target="_blank">How to format your data</a> </td>
				<td><button type="button" onclick="Javascript:document.getElementById('circos_div').innerHTML = '<img src=\'images/spinner2-greenie.gif\'   style=\'position:relative; left:0px;\' height=\'30\' width=\'30\' />'; window.setTimeout(function() { document.getElementById('circos_div').innerHTML = '<img src=\'images/example.png\' height=\'800\' width=\'800\' />'}, 15000);">Run Demo</button></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
	<!-- 
	Gene label data upload
	 -->
				<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Gene Label Data:</strong><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#geneLabels')">?</a>
					</div></td>
				<td name="phGene">placeholder</td>
				<td nowrap="nowrap"><div align="left">
						<strong>Link Lines:</strong><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#tilePlots')">?</a>
					</div>
				</td>
				<td name="phLink">placeholder</td>
				</tr>
<!-- 
Track 1 data upload
 -->
 				<tr>
 				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 1:</strong>
					</div></td>
				<td nowrap="nowrap"><select name='trackType1' id='trackType1'  onchange="Javascript: trackDownload('1'); connectPlotType('1');">
						<option value="NONE" >No track</option>
						<option value="Heatmap" selected ='selected'>Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<div id="track1_div"><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#heatmaps')">?&nbsp;&nbsp;</a><a href="Sampledata/exampleHeatmapData.txt">Download Data</a></div></td>
				<td name="ph1">placeholder
				</td>
				<td align="center"></td>
				</tr>
	<!-- 
	Track 2 data upload
	 -->	
	 			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 2:</strong>
					</div></td>
				<td><select name='trackType2' id='trackType2' onchange="Javascript: trackDownload('2'); connectPlotType('2');">
						<option value="NONE" >No track</option>
						<option value="Heatmap">Heatmap</option>
						<option value="Histogram" selected='selected'>Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				&nbsp; <div id="track2_div"><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#histograms')">?&nbsp;&nbsp;</a><a href="Sampledata/exampleHistogramData.txt">Download Data</a></div>
				</td>
				
				<td name="ph2">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>	
	<!-- 
	Track 3 data upload
	 -->
	 	 			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 3:</strong>
					</div></td>
				<td><select name='trackType3' id='trackType3' onchange = "Javascript: trackDownload('3'); connectPlotType('3');">
						<option value="NONE" >No track</option>
						<option value="Heatmap">Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line" selected='selected'>Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<div id="track3_div"><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#linePlots')">?&nbsp;&nbsp;</a><a href="Sampledata/exampleLineData.txt">Download Data</a></div>
				</td>
				<td name="ph3">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>	
			
			<!-- Track 4 data upload -->
			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 4:</strong>
					</div></td>
				<td><select name='trackType4' id='trackType4' onchange="Javascript: trackDownload('4'); connectPlotType('4');">
						<option value="NONE" >No track</option>
						<option value="Heatmap">Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter" selected ='selected'>Scatter Plot</option>
						<option value="Tile">Tile Plot</option>
				</select>
				<div id="track4_div"><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#scatterPlots')">?&nbsp;&nbsp;</a><a href="Sampledata/exampleScatterData.txt">Download Data</a></div>
				</td>
				<td name="ph4">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<!-- Track 5 data upload -->
			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Track 5:</strong>
					</div></td>
				<td><select name='trackType5' id='trackType5' onchange="Javascript: trackDownload('5'); connectPlotType('5');">
						<option value="NONE" >No track</option>
						<option value="Heatmap" >Heatmap</option>
						<option value="Histogram">Histogram</option>
						<option value="Line">Line Plot</option>
						<option value="Scatter">Scatter Plot</option>
						<option value="Tile" selected ='selected'>Tile Plot</option>
				</select>
				<div id="track5_div"><a href = "rcircos_help.html" onclick="Javascript: return popitup('rcircos_help.html#tilePlots')">?&nbsp;&nbsp;</a><a href="Sampledata/exampleTileData.txt">Download Data</a></div>
				</td>
				<td name="ph5">placeholder</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<!-- 
			Dimensions
			 -->

			<tr>
				<td>&nbsp;</td>
				<td><div align="right">
						<strong>Width/Height(in):</strong>
					</div>
				</td>
				<td><input name="width" type="text" id="width" value=6><br>
				</td>
				<td><div align="left">
						<strong>Track Width:</strong> (*chr width) <input
							name="trackHeight" type="text" id="trackHeight" value=1 onchange="Javascript: fixHeight()"><br>
					</div>
				</td>
				<td id="history"></td>

			</tr>
			<!-- 
	Link lines data upload
	 -->
			<tr>
				<td>&nbsp;</td>
				<td><strong>File Type:</strong><select name='fileType' id='fileType'>
						<option value="png" >png</option>
						<option value="jpeg" >jpeg</option>
						<option value="pdf">pdf</option>
				</select>
				 </td>
				<td nowrap="nowrap"><div align="left"><strong>Eliminate spacing</strong><input type="checkbox" name="spacing" id="spacing" checked align="left"></div> </td>
				<td><a
					onclick="Javascript:  var dis=document.getElementById('chrInclude'); im = document.getElementById('img_1'); if(dis.style.display == 'block'){ dis.style.display = 'none'; im.src='images/expand.gif'; } else { dis.style.display = 'block'; im.src='images/collapse.gif';};"
					href="Javascript: void(0);"> Chr: Include <img id="img_1"
						width="13" hspace="5" height="13" border="0" align="absmiddle"
						name="img_1" src="images/expand.gif"></img>
				</a>
					<div id="chrInclude"
						style="display: none; position: relative; left: 20px; border: thin none; background-color: rgb(238, 238, 238);">
						<table>
							<tbody>
								<tr>
									<td><input id="chrList[]" type="checkbox" value="chr1"
										name="chrList[]" checked></input> Chr1</td>
									<td><input id="chrList[]" type="checkbox" value="chr2"
										name="chrList[]" checked></input> Chr2</td>
									<td><input id="chrList[]" type="checkbox" value="chr3"
										name="chrList[]" checked></input> Chr3</td>
									<td><input id="chrList[]" type="checkbox" value="chr4"
										name="chrList[]" checked></input> Chr4</td>
									<td><input id="chrList[]" type="checkbox" value="chr5"
										name="chrList[]" checked></input> Chr5</td>
									<td><input id="chrList[]" type="checkbox" value="chr6"
										name="chrList[]" checked></input> Chr6</td>
									<td><input id="chrList[]" type="checkbox" value="chr7"
										name="chrList[]" checked></input> Chr7</td>
									<td><input id="chrList[]" type="checkbox" value="chr8"
										name="chrList[]" checked></input> Chr8</td>
								</tr>
								<tr>
									<td><input id="chrList[]" type="checkbox" value="chr9"
										name="chrList[]" checked></input> Chr9</td>
									<td><input id="chrList[]" type="checkbox" value="chr10"
										name="chrList[]" checked></input> Chr10</td>
									<td><input id="chrList[]" type="checkbox" value="chr11"
										name="chrList[]" checked></input> Chr11</td>
									<td><input id="chrList[]" type="checkbox" value="chr12"
										name="chrList[]" checked></input> Chr12</td>
									<td><input id="chrList[]" type="checkbox" value="chr13"
										name="chrList[]" checked></input> Chr13</td>
									<td><input id="chrList[]" type="checkbox" value="chr14"
										name="chrList[]" checked></input> Chr14</td>
									<td><input id="chrList[]" type="checkbox" value="chr15"
										name="chrList[]" checked></input> Chr15</td>
									<td><input id="chrList[]" type="checkbox" value="chr16"
										name="chrList[]" checked></input> Chr16</td>
								</tr>
								<tr>
									<td><input id="chrList[]" type="checkbox" value="chr17"
										name="chrList[]" checked></input> Chr17</td>
									<td><input id="chrList[]" type="checkbox" value="chr18"
										name="chrList[]" checked></input> Chr18</td>
									<td><input id="chrList[]" type="checkbox" value="chr19"
										name="chrList[]" checked></input> Chr19</td>
									<td id="Chr20"><input id="chrList[]" type="checkbox"
										value="chr20" name="chrList[]" checked></input> Chr20</td>
									<td id="Chr21"><input id="chrList[]" type="checkbox"
										value="chr21" name="chrList[]" checked></input> Chr21</td>
									<td id="Chr22"><input id="chrList[]" type="checkbox"
										value="chr22" name="chrList[]" checked></input> Chr22</td>
									<td><input id="chrList[]" type="checkbox" value="chrX"
										name="chrList[]" checked></input> ChrX</td>
									<td id="ChrY"><input id="chrList[]" type="checkbox"
										value="chrY" name="chrList[]" checked></input> ChrY</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>

				<td><div align="left">
						<strong>Plot name:</strong>(without file extension)<input name="plotName" type="text" id="plotName" onchange="Javascript: fixName();"><br>
					</div></td>
				
				<td>&nbsp;</td>
			</tr>
			<tr> <td colspan="6" align="center">
			<INPUT type="Submit" Name=Submit VALUE="Start" id="mySubmit">
          <input type="reset" value="Reset" onclick="Javascript: speciesReset()"/>        </td>
    </tr>
		</table>

</form>
<div id='circos_div' align='center'></div>
<div id='stop_div'><a style="display:none">huehuehue</a></div>
</body>
</html>
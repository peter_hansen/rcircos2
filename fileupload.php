<?php
//error_reporting(0);
$username = $_POST['userID'];
if(is_dir("temp/" . $username) == false | $username == '') {
	echo "<script> window.alert(\"ID does not match any sessions\");
		$('#start').css('display', 'none');";
	exit;
}
$outDir="temp/" . $username ."/";
if($_FILES["file1"]["tmp_name"]=='') {
	$file1 = '';
} else {
	$file1 = $outDir . $_FILES["file1"]["name"];
	move_uploaded_file($_FILES["file1"]["tmp_name"], $file1);
}
if($_FILES["file2"]["tmp_name"]=='') {
	$file2 ='';
} else {
	$file2 = $outDir . $_FILES["file2"]["name"];
	move_uploaded_file($_FILES["file2"]["tmp_name"], $file2);
}
if($_FILES["file3"]["tmp_name"]=='') {
	$file3 ='';
} else {
	$file3 = $outDir . $_FILES["file3"]["name"];
	move_uploaded_file($_FILES["file3"]["tmp_name"], $file3);
}
if($_FILES["file4"]["tmp_name"]=='') {
	$file4='';
} else {
	$file4 = $outDir . $_FILES["file4"]["name"];
	move_uploaded_file($_FILES["file4"]["tmp_name"], $file4);
}
if($_FILES["file5"]["tmp_name"]=='') {
	$file5='';
} else {
	$file5 = $outDir . $_FILES["file5"]["name"];
	move_uploaded_file($_FILES["file5"]["tmp_name"], $file5);
}
if($_FILES["file6"]["tmp_name"]==''){
	$file6 = '';
} else {
	$file6 =$outDir . $_FILES["file6"]["name"];
	move_uploaded_file($_FILES["file6"]["tmp_name"], $file6);
}
if($_FILES["file7"]["tmp_name"] =='') {
	$file7 = '';
} else {
	$file7 = $outDir . $_FILES["file7"]["name"];
	move_uploaded_file($_FILES["file7"]["tmp_name"], $file7);
}
$c = 0;
$images = array();
foreach (glob($outDir . 'hist/*') as $allfile1) {
	$images[$c] = $allfile1;
	$c++;
}
$c = 0;
$fileConnectArray = array();
$geneFiles = array();
$linkFiles = array();
$heatmapFiles = array();
$histogramFiles = array();
$lineScatFiles = array();
$tileFiles = array();
$n1 = 0;
foreach (glob($outDir . '*') as $allfile1) {
	if(substr($allfile1, strlen($username) + 6) != 'hist' & substr($allfile1, strlen($username) + 6) != 'relations.txt' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzzz' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzz') {
		$n1 += 1;
	}
}
$n2 = 0;
foreach (glob($outDir . '/hist/*') as $allfile1) {
	$n2 += 1;
}
if($n1 > $n2) {
	$canvasNum = $n1;
} else {
	$canvasNum = $n2;
}
$rowNum = $canvasNum +1;
$canvasNum *= 80;
echo "<table id='fileManager' width=\"400\" align=\"center\" cellpadding='3' cellspacing='2' style='border: 1px solid #CCCCCC;background-color:#DEEBDC; '>
		<tr><td>&nbsp;</td><td>Filename</td><td>Date uploaded</td><td>Filesize (KB)</td><td rowspan=$rowNum><canvas width='300' height='$canvasNum' id='relationsCanvas'></canvas></td></tr>";
foreach (glob($outDir . '*') as $allfile1) {
	if(substr($allfile1, strlen($username) + 6) != 'hist' & substr($allfile1, strlen($username) + 6) != 'relations.txt' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzzz' & substr($allfile1, strlen($username)+6, -6) != 'zzzzzz') {
		$time = filemtime($allfile1);
		$class = substr($allfile1, 6 + strlen($username), -4);
		echo "<tr style='display: table-row'><td height='70' class='". $class ."'><input id='delList[]' type='checkbox' value=" .  $allfile1. "
				name='delList[]'></input></td>";
		echo "<td class='". $class ."'>" . substr($allfile1, strlen($username) + 6) . "</td>";
		echo "<td class='". $class ."'>" .date(j, $time). "/". date(n, $time). "/". date(y,$time). "</td>";
		echo "<td id='".$class."' class='". $class ."'>" . round(filesize($allfile1)/1024, 1) . "</td>";
		if (empty($images[$c]) == false) {
			$imgClass = substr($images[$c], 11 + strlen($username), -4);
			if(substr($images[$c], -3) == 'pdf') {
				echo "<td></td><td><a id='$images[c]' class='$imgClass' href='$images[$c]' target='_blank'>Download</a>";
			} else {
				echo "<td></td><td>" . "<a id='$images[$c]' class='$imgClass' href='$images[$c]' target='_blank'><img align='center' src='". $images[$c] ."' height='40' width='40'></a>";
			}
		}
		echo "</tr>";
		$q=FALSE;
		if('txt' == substr($allfile1, -3)) {
			$q=TRUE;
		}
		$delimiter = $q ? "\t" : ",";
		$fh = fopen($allfile1, 'r');
		$data = fgetcsv($fh, $delimiter);
		$data2 = explode("\t", $data[0]);
		if($data2[3] == 'Gene') {
			$geneFiles[$c] = $allfile1;
		} elseif ($data2[3] == 'GeneName') {
			$heatmapFiles[$c] = $allfile1;
		} elseif ($data2[3] == 'Data') {
			$histogramFiles[$c] = $allfile1;
		} elseif ($data2[3] == 'num.mark') {
			$lineScatFiles[$c] = $allfile1;
		} elseif (empty($data2[3]) && $data2[0] == 'Chromosome') {
			$tileFiles[$c] = $allfile1;
		} elseif (substr($data2[3], 0, 11) == 'Chromosome.') {
			$linkFiles[$c] = $allfile1;
		}
		$c++;
	} else if (substr($allfile1, strlen($username)+6, -6) == 'zzzzzzz' | substr($allfile1, strlen($username)+6, -6) == 'zzzzzz' | $allfile1 == 'hist') {
		if (empty($images[$c]) == false) {
			echo $seed2;
			$imgClass = substr($images[$c], 11 + strlen($username), -4);
			echo "<tr><td></td><td></td><td></td><td></td><td></td><td>" . "<a id='$images[$c]' class='$imgClass' href='$images[$c]' target='_blank'><img align='center' src='". $images[$c] ."' height='40' width='40'></a></tr>";
		}
	$c++;
	}
}
$histScript = '';
foreach (glob($outDir . 'hist/*') as $allfile1) {
	$filename = substr($allfile1, strlen($username) + 11);
	$histScript .= "<option value='$allfile1'>$filename</option>";
}
echo "<tr><td colspan = 8 align = \"center\"><button id=\"deleteFiles\" type=\"button\" onclick=\"Javascript: $('#idForm').ajaxSubmit({url:'delFiles.php', type:'post', target: '#delete_div'});\">Delete checked files</button>";
echo "</table>";
if(empty($heatmapFiles)) {
	$heatmapScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$heatmapScript .= "<option value=''>No file</option>";
	foreach ($heatmapFiles as $file) {
		$filename = substr($file, strlen($username) + 6);
		$heatmapScript .= "<option value='$file' selected='selected'>$filename</option>";
	}
}
if(empty($histogramFiles)) {
	$histogramScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$histogramScript .= "<option value=''>No file</option>";
	foreach ($histogramFiles as $file) {
		$filename = substr($file, strlen($username) + 6);
		$histogramScript .= "<option value='$file' selected='selected'>$filename</option>";
	}
}
if(empty($lineScatFiles)) {
	$lineScatScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$lineScatScript .="<option value=''>No file</option>";
	foreach ($lineScatFiles as $file) {
		$filename = substr($file, strlen($username) + 6);
		$lineScatScript .= "<option value='$file' selected='selected'>$filename</option>";
	}
}
if(empty($tileFiles)) {
	$tileScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$tileScript .= "<option value=''>No file</option>";
	foreach ($tileFiles as $file) {
		$filename = substr($file, strlen($username) + 6);
		$tileScript .= "<option value='$file' selected='selected'>$filename</option>";
	}
}
if(empty($geneFiles)) {
	$geneScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$geneScript .= "<option value=''>No file</option>";
	foreach ($geneFiles as $file) {
		$filename = substr($file, strlen($username) + 6);
		$geneScript .= "<option value='$file' selected='selected'>$filename</option>";
	}
}
if(empty($linkFiles)) {
	$linkScript .= "<option value='' selected='selected'>No files available</option>";
} else {
	$linkScript .= "<option value=''>No file</option>";
	foreach ($linkFiles as $file) {
		$filename = substr($file, strlen($username) + 6);
		$linkScript .= "<option value='$file' selected='selected'>$filename</option>";
	}
}
$script ="<script>var colorArray = new Array('blue', 'orange', 'green', '#336633', '#000000', '#FF9900', '#999900','#c2374c', '#CC9933', '#0000cc', '#336633', '#000000', '#FF9900', '#999900','#c2374c', '#CC9933', '#0000cc', '#336633', '#000000', '#FF9900', '#999900');
		var c = -1;";
if (is_file('temp/' . $username . '/relations.txt') == true) {
	$relations = file_get_contents('temp/' . $username . '/relations.txt');
	$relationsArray=explode("~", $relations);
	foreach ($relationsArray as $relation) {
		$script .= $relation;
		$script .= "
				c++;
				var figPos = $('.' + jq(relArray[0])).offset();
				var len = relArray.length;
				var title = relArray[0] + ': ';
				for (var i= 1; i < len; i++) {
					if(i !== 1){
						title = title + ', ';
					}
					title = title + relArray[i];
					var filei = $('#' + jq(relArray[i]));
					var pos1 = filei.offset();
					DrawLine(pos1.left + filei.width(), pos1.top + filei.height()/2, figPos.left, figPos.top, colorArray[c]);
				}
				$('.' + jq(relArray[0])).prop('title', title);
				";
	}
}
$script .="	var scripts = new Array(\"$heatmapScript\", \"$histogramScript\", \"$lineScatScript\", \"$tileScript\");
	function start() {
		var username = document.getElementById('userID').value;
		var dis = document.getElementById('circosForm');
		dis.style.display = 'block';
		dis = document.getElementById('idForm');
		dis.style.display = 'none';
		dis = document.getElementById('user_div');
		dis.innerHTML =  username;
		$('#username').val(username);

	$(document).ready(function() {
		$(\"[name='ph1']\").html(\"<select name='track1' id='track1'>" .$heatmapScript . "\");
		$(\"[name='ph2']\").html(\"<select name='track2' id='track2'>" .$histogramScript . "\");
		$(\"[name='ph3']\").html(\"<select name='track3' id='track3'>" .$lineScatScript . "\");
		$(\"[name='ph4']\").html(\"<select name='track4' id='track4'>" .$lineScatScript . "\");
		$(\"[name='ph5']\").html(\"<select name='track5' id='track5'>" .$tileScript . "\");
		$(\"[name='phGene']\").html(\"<select name='geneLabel' id='geneLabel'>" .$geneScript . "\");
		$(\"[name='phLink']\").html(\"<select name='linkLines' id='linkLines'>" .$linkScript . "\"); ";
	if ($histScript != '') {
		$script .= "
		$(\"#history\").html(\"History:(month:day:hour:minute:second)<select name='historySelect' id='historySelect' onchange='Javascript: histUpdate();'><option value='none'>No option selected</option>" .$histScript . "<!-- hue -->\"); ";
	}
$script .= "		});
	}
	function connectPlotType(num) {
	var trackType = 'trackType' + num;
	var trackPh = \"track\" + num;
	var doc = document.getElementById(trackType);
	var type = doc.options[doc.selectedIndex].value;
	if(type === 'Heatmap') {
		var i = 0;
	} else if(type === 'Histogram') {
		var i = 1;
	} else if(type === 'Line' | type === 'Scatter') {
		var i = 2;
	} else if(type === 'Tile') {
		var i = 3;
	}
	var newMenu = scripts[i];
	document.getElementById(trackPh).innerHTML = newMenu;
	}</script>";
echo $script;
?>